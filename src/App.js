import React from "react";
import Cytoscape from "cytoscape";
import CytoscapeComponent from "react-cytoscapejs";
import dagre from "cytoscape-dagre";

Cytoscape.use(dagre);

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const nodes = [
      { data: { id: "Account", label: "Account", type: "node-a" } },
      { data: { id: "Current", label: "Current", type: "node-b" } },
      { data: { id: "Savings", label: "Savings", type: "node-b" } },
      { data: { id: "Salary", label: "Salary", type: "node-b" } },
      { data: { id: "Normal", label: "Normal", type: "node-c"} },
      { data: { id: "Advantage", label: "Advantage", type: "node-c"} },
      { data: { id: "Youth", label: "Youth", type: "node-c"} },
      { data: { id: "Prestige", label: "Prestige", type: "node-c"} },
      { data: { id: "Prime", label: "Prime", type: "node-c"} },
      { data: { id: "Defence", label: "Defence", type: "node-c"} },
      { data: { id: "NRO", label: "NRO", type: "node-c"} },
      { data: { id: "Balance", label: "Balance", type: "node-d"} },
      { data: { id: "Mini Statement", label: "Mini Statement", type: "node-d"} },
      { data: { id: "Ledger Balance", label: "Ledger Balance", type: "node-d"} },
      { data: { id: "Transactions", label: "Transactions", type: "node-d"} },
      { data: { id: "Balance 2", label: "Balance", type: "node-d"} },
      { data: { id: "Mini Statement 2", label: "Mini Statement", type: "node-d"} },
      { data: { id: "Ledger Balance 2", label: "Ledger Balance", type: "node-d"} },
      { data: { id: "Transactions 2", label: "Transactions", type: "node-d"} },
    ];

    const edges = [
      { data: { source: "Account", target: "Current" } },
      { data: { source: "Account", target: "Savings" } },
      { data: { source: "Account", target: "Salary" } },
      { data: { source: "Current", target: "Normal" } },
      { data: { source: "Current", target: "Advantage" } },
      { data: { source: "Savings", target: "Youth" } },
      { data: { source: "Savings", target: "Prestige" } },
      { data: { source: "Salary", target: "Prime" } },
      { data: { source: "Salary", target: "Defence" } },
      { data: { source: "Salary", target: "NRO" } },
      { data: { source: "Salary", target: "Balance" } },
      { data: { source: "Salary", target: "Mini Statement" } },
      { data: { source: "Salary", target: "Ledger Balance" } },
      { data: { source: "Salary", target: "Transactions" } },
      { data: { source: "Youth", target: "Balance 2" } },
      { data: { source: "Youth", target: "Mini Statement 2" } },
      { data: { source: "Youth", target: "Ledger Balance 2" } },
      { data: { source: "Youth", target: "Transactions 2" } },
    ];
    const layout = { name: "dagre" };

    return (
      <CytoscapeComponent
      panningEnabled={true}
        autoungrabify={true}
        stylesheet={[
          {
            selector: "node",
            style: {
              label: "data(id)",
              height: "10px",
              width: "10px",
              'font-size': '7px',
              color:"#2b2e4a"
            }
          },
          {
            selector: "[type = 'node-a']",
            style: {
              "background-color": "#c06c84",
              label: "data(id)",
              height: "15px",
              width: "15px"
            }
          },
          {
            selector: "[type = 'node-b']",
            style: {
              "background-color": "#3f72af",
              label: "data(id)",
              height: "13px",
              width: "13px"
            }
          },
          {
            selector: "[type = 'node-c']",
            style: {
              "background-color": "#522546",
              label: "data(id)",
              height: "10px",
              width: "10px"
            }
          },
          {
            selector: "[type = 'node-d']",
            style: {
              "background-color": "#ff6f3c",
              label: "data(id)",
              height: "8px",
              width: "8px"
            }
          },
          {
            selector: "edge",
            style: {
              "line-color": '#d3d4d8',
              width: "1px"
            }
          }
        ]}
        elements={CytoscapeComponent.normalizeElements({
          nodes,
          edges
        })}
        layout={layout}
        style={{ width: "1450px", height: "900px" }}
      />
    );
  }
}

export default App;
